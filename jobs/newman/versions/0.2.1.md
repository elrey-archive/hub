* Update docker image version to `15.10.0`
* Fix version of `newman` package to `5.2.2` through variable `NEWMAN_VERSION`
* Fix version of `newman-reporter-junitfull` package to `1.1.1` through variable `NEWMAN_JUNIT_VERSION`