---
title: Terms of use
description: Find here the terms of use of R2Devops
---


## Presentation of the site

Under article 6 of Law No. 2004-575 of June 21, 2004 on confidence in the digital economy, users of the site www.r2devops.io are informed of the identity of the various parties involved in the context of its realization and its follow-up:

  - Owner: Go2Scale – SAS N ° 880 851 126 – 222 place Ernest Granier 34000 MONTPELLIER
  - Creator: Go2Scale
  - Capital: 6000 €
  - Publication manager: Go2Scale – contact@go2scale.com. The publication manager is a natural person or a legal person.
  - Webmaster: Go2Scale – contact@go2scale.com
  - Host: GitLab Inc, 268 Bush Street #350, San Francisco, CA 94104, United States of America

## General conditions of use of the site and the services offered
The use of the site www.r2devops.io implies full acceptance of the general conditions of use described below. These conditions of use may be modified or supplemented at any time, users of the www.r2devops.io site are therefore invited to consult them regularly.
This site is normally accessible to users at all times.
The www.r2devops.io site is updated regularly. In the same way, the legal notices can be modified at any time: they nevertheless impose themselves on the user who is invited to refer to them as often as possible in order to read them.

## Description of services provided

www.r2devops.io is a DevOps As a Service platform.
R2Devops provides services around DevOps methodologies.
We strive to provide as accurate information as possible on the www.r2devops.io website. However, it cannot be held responsible for omissions, inaccuracies and deficiencies in the update, whether they be its own fault or that of the third party partners who provide it with this information.
All the information indicated on the site www.r2devops.io are given as an indication, and are likely to evolve. Furthermore, the information on the site www.r2devops.io is not exhaustive. They are given subject to modifications having been made since they were put online.

### Use R2Devops HUB
There is no limit on the use of resources (jobs) from the HUB. However, you have to respect the License applied on each resource.
Jobs are free opensource and free to use.

### Contribute to R2Devops HUB
The HUB is collaborative, you are free to contribute and propose idea on the existing resources.
You can also contribute by providing your own resource to be include in the HUB [see linked repository](#contribute-by-linking-a-contributor-repository-hub)

#### Contribute to the official HUB repository
Contributors can contribute in the official repository.
All resources hosted in the R2Devops official repository belong to the company.
All resources provided in the official repository undergo a specific treatment (Go2Scale integration Pipeline) to be included in the official R2Devops Hub and guarantee a high level of quality.

#### Contribute by linking a contributor repository
Contributors can also decide to link their own repositories to the R2Devops HUB.
In this case, resources belong to the contributor.
All resources available through a linked repository don't have any mandatory qualities criteria.
It's up to the contributor to define the rules and the quality of the resource he provides.
The company cannot be help responsible for any issue met using a resource from a linked repository.

## Contractual limitations on technical data
The site uses JavaScript technology.
The website cannot be held responsible for material damage linked to the use of the site. In addition, the user of the site agrees to access the site using recent equipment, not containing any virus and with an up-to-date latest generation browser.

## Intellectual property and counterfeits
Some resources are usable under license. The visitor commits to respect the License of the resource used.
Go2Scale owns the intellectual property rights or holds the usage rights on all the elements accessible on the site, in particular the texts, images, graphics, logo, icons, sounds, software.
Any reproduction, representation, modification, publication, adaptation of all or part of the elements of the site, whatever the means or process used, is prohibited without the prior written permission of the company.
Any unauthorized exploitation of the site or any of the elements it contains will be considered as constituting an infringement and prosecuted in accordance with the provisions of articles L.335-2 and following of the Intellectual Property Code.

## Limitations of liability
Go2Scale cannot be held responsible for direct and indirect damage caused to the user’s equipment, when accessing the www.r2devops.io site, and resulting either from the use of equipment that does not meet the specifications indicated in point 4, either the appearance of a bug or an incompatibility.
Go2Scale cannot also be held liable for indirect damage (such as loss of market or loss of opportunity) resulting from the use of the services provided by www.r2devops.io.

## Applicable law and attribution of jurisdiction
Any dispute in connection with the use of the site www.r2devops.io is subject to French law. Exclusive jurisdiction is given to the competent courts of Montpellier.

## The main laws concerned
Law n ° 78-17 of January 6, 1978, notably modified by law n ° 2004-801 of August 6, 2004 relating to data processing, files and freedoms.
Law n ° 2004-575 of June 21, 2004 for confidence in the digital economy.

## Lexicon
User: Internet user connecting, using the aforementioned site.
Personal information: “information that allows, in any form whatsoever, directly or indirectly, the identification of the natural persons to whom it applies” (article 4 of law n ° 78-17 of January 6, 1978).

## Credits
The model of legal notices is offered by Subdelirium.com Generator of legal notices.     


--8<-- "includes/abbreviations.md"
