---
title: Collaborative hub of CI/CD jobs ready to use
description: Discover R2Devops and learn about the goal of the hub. See how CI/CD implementation can be simplified using this platform!
---

# Concept

The **R2Devops hub** is a collaborative [Hub](/r2bulary/#hub) of CI & CD
**ready to use** jobs which helps you to easily build powerful Pipelines for your projects.

!!! info
    Currently, the hub is focused to provide only **Gitlab 🦊** jobs. We plan
    to support more CI/CD platforms in the future.

Each Job of the hub can be used independently to create fully **customized pipelines.**
You can use them for any kind of software and deployment type. Each job can be
customized through configuration.

<a alt="Use the hub" href="/use-the-hub">
    <button class="md-button border-radius-10 md-button-center" >
        I want to use the hub <img alt="" class="heart" src="../images/rocket.png">
    </button>
</a>


## Overview

![hub overview](images/g2shub_mvp.jpg)

--8<-- "includes/abbreviations.md"
